//testing if linked list insertion even works

#include <iostream> //cin and cout

#include <fstream> //file processing

#include <sstream> //formatted string processing

#include <cstdlib> //atof and atoi

#include "student.hpp"




int main()
{




//Read the domestic-stu.txt file and exit if failed

string dom_line;

ifstream domesticFile("domestic-stu.txt");

if(!domesticFile.is_open()) {

cout << "Unable to open file domestic-stu.txt" << endl;

return -1;

}

//declare the necessary variables as a string

string firstName, lastName, province, s_cgpa, s_researchScore;

float cgpa;

int researchScore;

domstudptr head, tmp, tail=NULL;
//head = new DomesticStudent(NULL, " "," "," ",0,0,0);
//
//
//head = new DomesticStudent(NULL, NULL, NULL,NULL,NULL,NULL,NULL);
int dom_stu_count = 1;

while( getline(domesticFile, dom_line) ) {

/* if(*head==NULL)

{
	cout<<"null head";
	return 0;
}
*/

//if (head->getLink()==NULL)
//{
//	head =new DomesticStudent(head, firstName, lastName, province, cgpa, researchScore, (20200000 + dom_stu_count));

istringstream ss(dom_line);




//string firstName, lastName, province, s_cgpa, s_researchScore;

//float cgpa;

//int researchScore;




//get firstName separated by comma

getline(ss, firstName, ',');




//get lastName separated by comma

getline(ss, lastName, ',');




//get province separated by comma

getline(ss, province, ',');




//get cpga separated by comma, and convert string to float

getline(ss, s_cgpa, ',');

cgpa = atof(s_cgpa.c_str());




//get researchScore separated by comma, and convert it to int

getline(ss, s_researchScore, ',');

researchScore = atoi(s_researchScore.c_str());



 //createNode(firstName, lastName, province, cgpa, researchScore, (20200000 + dom_stu_count));

domstudptr new_node=createNode(firstName, lastName, province, cgpa, researchScore, (20200000 + dom_stu_count));

sortedInsert(&head, new_node);





dom_stu_count++; 

}


printList(head);




/*
//displaying function
tmp=head;

while (tmp!=NULL)

{
//i dont know what the point of this part is
cout<<tmp->getFN() <<endl;

tmp=tmp->getLink();

 

}

tmp=head;

while (tmp !=NULL)

{

domstudptr nodeToDelete = tmp;
tmp=tmp->getLink();
delete nodeToDelete;

}*/


char uiStudentChoice;
char uiFunctionChoice;
char uiSearchChoice;
int uiSearchID;
double uiSearchCGPA;
int uiSearchRS;
string uiSearchFN;
string uiSearchLN;
string uiDeleteFN;
string uiDeleteLN;

 

//user picks Domestic Students or International Students
cout << "Would you like to work with Domestic Students or International Students? Enter the appropriate letter." << endl;
cout << "D: Domestic Students" << endl << "I: International Students" <<  endl;
 cout << "Enter: ";
cin >> uiStudentChoice;

//if user picks Domestic Students
if (uiStudentChoice == 'D' || uiStudentChoice == 'd')
{
  cout << "What would you like to do with Domestic Students? Enter the appropriate letter." << endl;
  cout << "S: Search" << endl << "C: Create" << endl << "D: Delete" << endl;
  cout << "Enter: ";
  cin >> uiFunctionChoice;
  
  if (uiFunctionChoice == 'S' || uiFunctionChoice == 's')
  {
    //search
    cout << "What would you like to search? Enter the appropriate letter." << endl;
    cout << "A: Application ID" << endl << "C: CGPA" << endl << "R: Research Score" << endl << "N: Name" << endl;
  cout << "Enter: ";
    cin >> uiSearchChoice;
    
    if (uiSearchChoice == 'A' || uiSearchChoice == 'a')
    {
      //search app id
      cout << "Search Application ID: ";
      cin >> uiSearchID;
 //////

       DomesticStudent *here;
       here = head;
        if(here == NULL) //if the pointer initially points to the tail that means the list is empty
        {
            cout << "Error. Empty list." << endl;
        }
	while(here->getLink()->getID() != uiSearchID && here != NULL) //while the pointer is not at the target data or at the end of the list, continue searching the list
        {
	        here = here->getLink();
		if(here->getID() == uiSearchID) //if targetted data is found, output targetted data and continue searching the list
                    {
		      cout << here << endl; 
			      cout<<here->getLink()->getProvince()<<" "<<here->getLink()->getFN()<<" "<<here->getLink()->getLN()<<" "<<here->getLink()->getCGPA()<<" "<<here->getLink()->getResearch()<<endl;
		      // here = here->getLink();
                    }			  // here = here->getLink();
		else if(here->getLink() == NULL)//if the pointer is at the end of the list, last real node before tail
                    {
		      if(here->getID() == uiSearchID) //check if targetted data is found, output targetted data
			{
			  cout << here << endl; 
	 cout<<here->getLink()->getProvince()<<" "<<here->getLink()->getFN()<<" "<<here->getLink()->getLN()<<" "<<here->getLink()->getCGPA()<<" "<<here->getLink()->getResearch()<<endl;
			  // here = here->getLink();
			}
		      else 
                        cout << "All students with matching application ID's are listed above." << endl;
                    }
        }
//////
      //check for matching ID
      //print found students or no match
    }
    else if (uiSearchChoice == 'C' || uiSearchChoice == 'c')
    {
      //search cgpa
      cout << "Search CGPA: ";
      cin >> uiSearchCGPA;
      //check for matching CGPA
      //print found students or no match
      DomesticStudent *here;
       here = head;
        if(here == NULL) //if the pointer initially points to the tail that means the list is empty
        {
            cout << "Error. Empty list." << endl;
        }
	while(here->getLink()->getCGPA() != uiSearchCGPA && here != NULL) //while the pointer is not at the target data or at the end of the list, continue searching the list
        {
	        here = here->getLink();
		if(here->getCGPA() == uiSearchCGPA) //if targetted data is found, output targetted data and continue searching the list
                    {
		      cout << here << endl; 
		      cout<<here->getLink()->getProvince()<<" "<<here->getLink()->getFN()<<" "<<here->getLink()->getLN()<<" "<<here->getLink()->getCGPA()<<" "<<here->getLink()->getResearch()<<endl;
		      // here = here->getLink();
                    }			  // here = here->getLink();
		else if(here->getLink() == NULL)//if the pointer is at the end of the list, last real node before tail
                    {
		      if(here->getCGPA() == uiSearchCGPA) //check if targetted data is found, output targetted data
			{
			  cout << here << endl; 
			      cout<<here->getLink()->getProvince()<<" "<<here->getLink()->getFN()<<" "<<here->getLink()->getLN()<<" "<<here->getLink()->getCGPA()<<" "<<here->getLink()->getResearch()<<endl;
			  // here = here->getLink();
			}
		      else 
                        cout << "All students with matching application ID's are listed above." << endl;
                    }
        }
    }
    else if (uiSearchChoice == 'R' || uiSearchChoice == 'r')
    {
      //search research score
      cout << "Search Research Score: ";
      cin >> uiSearchRS;
      //check for matching research score
      //print found students or no match
      DomesticStudent *here;
       here = head;
        if(here == NULL) //if the pointer initially points to the tail that means the list is empty
        {
            cout << "Error. Empty list." << endl;
        }
	while(here->getLink()->getResearch() != uiSearchRS && here != NULL) //while the pointer is not at the target data or at the end of the list, continue searching the list
        {
	        here = here->getLink();
		if(here->getResearch() == uiSearchRS) //if targetted data is found, output targetted data and continue searching the list
                    {
		      cout << here << endl; 
		      cout<<here->getLink()->getProvince()<<" "<<here->getLink()->getFN()<<" "<<here->getLink()->getLN()<<" "<<here->getLink()->getCGPA()<<" "<<here->getLink()->getResearch()<<endl;
		      // here = here->getLink();
                    }			  // here = here->getLink();
		else if(here->getLink() == NULL)//if the pointer is at the end of the list, last real node before tail
                    {
		      if(here->getResearch() == uiSearchRS) //check if targetted data is found, output targetted data
			{
			  cout << here << endl; 
			      cout<<here->getLink()->getProvince()<<" "<<here->getLink()->getFN()<<" "<<here->getLink()->getLN()<<" "<<here->getLink()->getCGPA()<<" "<<here->getLink()->getResearch()<<endl;
			  // here = here->getLink();
			}
		      else 
                        cout << "All students with matching application ID's are listed above." << endl;
                    }
        }
    }
    else if (uiSearchChoice == 'N' || uiSearchChoice == 'n')
    {
      //search name
      cout << "Search First Name: ";
      cin >> uiSearchFN;
      cout << endl << "Search Last Name: ";
      cin >> uiSearchLN;
      //check for matching FULL name
      //print found students or no match
      cout << "Hey. :) We did not get to this part, but we tried very very hard :'(." << endl << "Hope you enjoyed finding this secret message. Have a great day/night!"<<endl;
    }
    else
    {//if user did not enter appropriate letter for search choice
      //invalid input
      cout << "Invalid input. Exiting." << endl;
      return 0;
    }
  }
  else if (uiFunctionChoice == 'C' || uiFunctionChoice == 'c')
  {
    //create
    cout << "Create New Domestic Student." << endl;
    //cin all variables
    //insert node in SORTED linked list
  }
  else if (uiFunctionChoice == 'D' || uiFunctionChoice == 'd')
  {
    //delete
    cout << "Delete Domestic Student." << endl;
    cout << "Delete Student First Name: ";
    cin >> uiDeleteFN;
    cout << "Delete Student Last Name: ";
    cin >> uiDeleteLN;
    // find matching student
    //delete matching object
  }
  else
  {//if user did not enter appropriate letter for function choice
    cout << "Invalid input. Exiting." << endl;
    return 0;
  }
}
//if user picks International Students
else if (uiStudentChoice == 'I' || uiStudentChoice == 'i')
{
  cout << "What would you like to do with International Students? Enter the appropriate letter." << endl;
  cout << "S: Search" << endl << "C: Create" << endl << "D: Delete" << endl;
  cout << "Enter: ";
  cin >> uiFunctionChoice;
  
  if (uiFunctionChoice == 'S' || uiFunctionChoice == 's')//if use picks search
  {
    //search
    cout << "What would you like to search? Enter the appropriate letter." << endl;
    cout << "A: Application ID" << endl << "C: CGPA" << endl << "R: Research Score" << "N: Name" << endl;
    cout << "Enter: ";
    cin >> uiSearchChoice;
    
    if (uiSearchChoice == 'A' || uiSearchChoice == 'a')//if user picks search application ID
    {
      //search app id
      cout << "Search Application ID: ";
      cin >> uiSearchID;
      //check for matching ID
      //print found students or no match
    }
    else if (uiSearchChoice== 'C' || uiSearchChoice == 'c')//if user picks search cgpa
    {
      //search cgpa
      cout << "Search CGPA: ";
      cin >> uiSearchCGPA;
      //check for matching CGPA
      //print found students or no match
    }
    else if (uiSearchChoice == 'R' || uiSearchChoice == 'r')//if user picks search research score
    {
      //search research score
      cout << "Search Research Score: ";
      cin >> uiSearchRS;
      //check for matching research score
      //print found students or no match
    }
    else if (uiSearchChoice == 'N' || uiSearchChoice == 'n')//if user picks search by name
    {
      //search name
      cout << "Search First Name: ";
      cin >> uiSearchFN;
      cout << endl << "Search Last Name: ";
      cin >> uiSearchLN;
      //check for matching name
      //print found students or no match
    }
    else//if user did not enter appropriate letter for search choice
    {
      //invalid input
      cout << "Invalid input. Exiting." << endl;
      return 0;
    }
  }
  else if (uiFunctionChoice == 'C' || uiFunctionChoice == 'c')//if user chose to create
  {
    //create
    cout << "Create New Domestic Student." << endl;
    //cin all variables
    //insert node in linked list
    //sort updated linked list
  }
  else if (uiFunctionChoice == 'D' || uiFunctionChoice == 'd')//if user chose to delete
  {
    //delete
    cout << "Delete Domestic Student." << endl;
    cout << "Delete Student First Name: ";
    cin >> uiDeleteFN;
    cout << "Delete Student Last Name: ";
    cin >> uiDeleteLN;
    cout << endl;
    //delete matching object
  }
  else//if user did not enter appropriate letter for function choice
  {
    cout << "Invalid input. Exiting." << endl;
    return 0;
  }
}
//if invalid input
else//if user did not enter appropriate letter for student choice
{
    cout << "Invalid input. Exiting." << endl;
    return 0;
}

return 0;

}
