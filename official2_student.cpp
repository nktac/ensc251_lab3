//student.cpp to implement your classes
#include "student.hpp"
#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;

//below is a series of set functions for the classes

void Student::setID(int student_number) 
{
	id_number = student_number; //set ID numbers
}

void Student::setFN(string fn) 
{
	first_name = fn; //set First Names
}

void Student::setLN(string ln) 
{
	last_name = ln; //set Last Names
}

void Student::setCGPA(float cg) 
{
//perform basic error checking to make sure the cgpa is in the range 0<=cgpa<=4.33
//if invalid, output an error statement and terminate the program
	if (cg >= 0 && cg <= 4.33) 
	{
	//if valid, store the value into the array
	student_cgpa = cg; //Set CGPAs
	}
	else 
	{	
		//if
		cout << endl << "Student has an invalid cgpa, correct file and run again.\n";
		exit(0);
	}
}

void Student::setResearch(int re) 
{
//perform basic error checking to make sure the research score is in the range 0<=score<=100
//if invalid, output an error statement and terminate the program
	if (re >= 0 && re <= 100) 
	{
	//if valid, store the value into the array
	research_score = re; //Set Research Scores
	}
	else 
	{
	//if invalid, print error and terminate
	cout << endl << "Student has an invalid research score, correct file and run again.\n";
	exit(0);
	}
}


//Get functions for the student class, returns requested value for the specified student number:
int Student::getID() 
{
	return id_number; //Return ID Numbers
}

string Student::getFN() 
{
	return first_name; //Return First Names
}

string Student::getLN() 
{
	return last_name; //Return Last Names
}

float Student::getCGPA() 
{
	return student_cgpa; //Return CGPAs
}

int Student::getResearch() 
{
	return research_score; //Return Research Scores
}



// FRIEND FUNCTIONS
int compareCGPA(Student cgpa1, Student cgpa2) 
{
//declare three returns options: greater (1), smaller (-1) or equal (0)
//between the two students' CGPA
	if (cgpa1.getCGPA() == cgpa2.getCGPA()) 
	{
		return 0;
	}
	else if (cgpa1.getCGPA() > cgpa2.getCGPA()) 
	{
		return 1;
	}
	else 
	{
		return -1;
	}
}


int compareResearchScore(Student research1, Student research2) 
{
//declare three returns options: greater (1), smaller (-1) or equal (0)
//between the two students' Research scores
	if (research1.getResearch() == research2.getResearch()) 
	{
		return 0;
	}
	else if (research1.getResearch() > research2.getResearch()) 
	{
		return 1;
	}
	else 
	{
		return -1;
	}
}


//The compare functions for first name, last name, province, and country account for capitalization differences by doing the following:
//For the length of each string, check each character to see if it's upper or lower case, if it is upper case then convert it to lower case.
//Upper case have corresponding ASCII of <=90, and lower case have <=90 + 32
//(This is done by adding 32, since 32 corresponds to the integer difference between upper and lower case numbers in the ASCII table)
//By accounting for this, strings in all lower, all upper, or assorted cases are all sorted properly.
int compareFirstName(Student FN1, Student FN2) 
{ //First name using method decribed above
	string strF1 = FN1.getFN();
	string strF2 = FN2.getFN();
	for (int i = 0; i < strF1.size(); i++) 
	{
		if (strF1.at(i) <= 90) 
		{
			strF1.at(i) = strF1.at(i) + 32;
		}
	}
	for (int i = 0; i < strF2.size(); i++) 
	{
		if (strF2.at(i) <= 90) 
		{
			strF2.at(i) = strF2.at(i) + 32;
		}
	}
	if (strF1 == strF2) 
	{
		return 0;
	}
	else if (strF1 > strF2) 
	{
		return -1;
	}
	else 
	{
		return 1;
	}
}


int compareLastName(Student LN1, Student LN2) 
{ //Last name using method decribed above
	string strL1 = LN1.getLN();
	string strL2 = LN2.getLN();
	
	for (int i = 0; i < strL1.size(); i++) 
		{
			if (strL1.at(i) <= 90) 
			{
				strL1.at(i) = strL1.at(i) + 32;
			}
		}
	for (int i = 0; i < strL2.size(); i++) 
	{
		if (strL2.at(i) <= 90) 
		{
			strL2.at(i) = strL2.at(i) + 32;
		}
	}
	if (strL1 == strL2) 
	{
		return 0;
	}
	else if (strL1 > strL2) 
	{
		return -1;
	}
	else 
	{
		return 1;
	}
}


int compareProvince(DomesticStudent P1, DomesticStudent P2) { //Province using method decribed above
string strP1 = P1.getProvince();
string strP2 = P2.getProvince();
for (int i = 0; i < strP1.size(); i++)
{
if (strP1.at(i) <= 90) 
{
strP1.at(i) = strP1.at(i) + 32;
}
}
for (int i = 0; i < strP2.size(); i++) 
{
if (strP2.at(i) <= 90) 
{
strP2.at(i) = strP2.at(i) + 32;
}
}
if (strP1 == strP2) 
{
return 0;
}
else if (strP1 > strP2) 
{
return -1;
}
else 
{
return 1;
}
}


int compareCountry(InternationalStudent C1, InternationalStudent C2) { //Country using method decribed above
string strC1 = C1.getCountry();
string strC2 = C2.getCountry();
for (int i = 0; i < strC1.size(); i++) 
{
if (strC1.at(i) <= 90) 
{
strC1.at(i) = strC1.at(i) + 32;
}
}
for (int i = 0; i < strC2.size(); i++) 
{
if (strC2.at(i) <= 90) 
{
strC2.at(i) = strC2.at(i) + 32;
}
}
if (strC1 == strC2) 
{
return 0;
}
else if (strC1 > strC2) 
{
return -1;
}
else {
return 1;
}
}
/*

//We created a bubblesort function for both domestic students and international students separately.
//For each class of students that are stored as objects in the array we have a length and a flag.
//The flag correspondes to five sorting methods, described as follows:
void DomBubbleSort(DomesticStudent domArr[], size_t length, size_t flag)
{
for (size_t i = 0; i < length; i++ )
{
//Loop through the entire array, subtracting length by i and 1 since after every cycle the smallest number
//gets pushed to the last entry and is no longer considered.
for (size_t j = 0; j < length - 1 - i; j++)
{
if (flag == 1) //flag 1 means sorting them by research scores first
{
	if(compareResearchScore(domArr[j], domArr[j+1]) == -1)
	{
	//if the score is greater than the score in the next array entry
	//swap
	DomesticStudent buffer;
	buffer = domArr[j];
	domArr[j] = domArr[j+1];
	domArr[j+1] = buffer;
	}
	else if(compareResearchScore(domArr[j], domArr[j+1]) == 0)
	{
	//if they have the same research scores then sort them by CGPAs
		if (compareCGPA(domArr[j], domArr[j+1]) == -1)
		{
		//if the score is greater than the score in the next array entry
		//swap
		DomesticStudent buffer;
		buffer = domArr[j];
		domArr[j] = domArr[j+1];
		domArr[j+1] = buffer;
		}
		else if(compareCGPA(domArr[j], domArr[j+1]) == 0)
		{
		//if they further have the same CGPA then sort them by province
		if (compareProvince(domArr[j], domArr[j+1]) == -1)
		{
		//if the score is greater than the score in the next array entry
		//swap
		DomesticStudent buffer;
		buffer = domArr[j];
		domArr[j] = domArr[j+1];
		domArr[j+1] = buffer;
		}
		}
}
}
else if (flag == 2) //flag 2 correspondes to only sorting the domestic
//students by their CGPAs
{
if (compareCGPA(domArr[j], domArr[j+1]) == -1)
{
//if the score is greater than the score in the next array entry
//swap
DomesticStudent buffer;
buffer = domArr[j];
domArr[j] = domArr[j+1];
domArr[j+1] = buffer;
}
}
else if (flag == 3)
{
//flag 3 correspndes to only sorting the domestic
//students by their research scores
if(compareResearchScore(domArr[j], domArr[j+1]) == -1)
{
DomesticStudent buffer;
buffer = domArr[j];
domArr[j] = domArr[j+1];
domArr[j+1] = buffer;
}
}
else if (flag == 4)
{
//flag 3 correspndes to only sorting the domestic
//students by their first names
if (compareFirstName(domArr[j], domArr[j+1]) == -1)
{
DomesticStudent buffer;
buffer = domArr[j];
domArr[j] = domArr[j+1];
domArr[j+1] = buffer;
}
}
else if (flag == 5)
{
//flag 3 correspndes to only sorting the domestic
//students by their last names
if (compareLastName(domArr[j], domArr[j+1]) == -1)
{
DomesticStudent buffer;
buffer = domArr[j];
domArr[j] = domArr[j+1];
domArr[j+1] = buffer;
}
}
else
{
//if the flag is not recognized, output an error statement
cout << "Invalid sorting flag, please try again " << endl;
return;
}
}
//}
}


//Using an int function to return the new length of the sorted array (if overall sort)
int IntBubbleSort(InternationalStudent intArr[], unsigned long int length, unsigned long int flag)
{
//for international student total sort, we need to cut the students that cannot meet
//certain requirements
if(flag == 1)
{
int lCount = 0; //Counter to keep track of how many students we cut
for (int z = 0; z < length-lCount; z++) 
{
//the requirements that must be met:
if(intArr[z].personalScore.getToefl() < 93 || intArr[z].personalScore.getReading() < 20 || intArr[z].personalScore.getWriting() < 20 || intArr[z].personalScore.getSpeaking()
< 20 || intArr[z].personalScore.getListening() < 20)
{
//the following for loop shifts every array elements up by one place (after the one that doesn't meet),
//except for the one that doesn't meet the requirements, which is therefore deleted
for (int k=z; k<length-1; k++) 
{
intArr[k] = intArr[k+1];
}
z--; //Subtracting z to re-consider the same array entry that has now been shifted to.
lCount++; //Increasing the counter for cut students
}
}
length = length - lCount; //the total length number decreases by the students removed
}
//perform the same loop as the domestic students
for (int i = 0; i < length; i++ )
{
//Loop through the entire array, subtracting length by i and 1 since after every cycle the smallest number
//gets pushed to the last entry and is no longer considered.
for (int j = 0; j < length - 1 - i; j++)
{
if (flag == 1)
{
if(compareResearchScore(intArr[j], intArr[j+1]) == -1)
//if the score is greater than the score in the next array entry
//swap
{
InternationalStudent buffer;
buffer = intArr[j];
intArr[j] = intArr[j+1];




intArr[j+1] = buffer;




}




else if(compareResearchScore(intArr[j], intArr[j+1]) == 0)




{




if (compareCGPA(intArr[j], intArr[j+1]) == -1)




{




//if the score is greater than the score in the next array entry




//swap




InternationalStudent buffer;




buffer = intArr[j];




intArr[j] = intArr[j+1];




intArr[j+1] = buffer;




}




else if(compareCGPA(intArr[j], intArr[j+1]) == 0)




{




if (compareCountry(intArr[j], intArr[j+1]) == -1)




{




InternationalStudent buffer;




buffer = intArr[j];




intArr[j] = intArr[j+1];




intArr[j+1] = buffer;




}




}




}




}




else if (flag == 2)




{




if (compareCGPA(intArr[j], intArr[j+1]) == -1)




{




InternationalStudent buffer;




buffer = intArr[j];




intArr[j] = intArr[j+1];




intArr[j+1] = buffer;




}




}




else if (flag == 3)




{




if(compareResearchScore(intArr[j], intArr[j+1]) == -1)




{




InternationalStudent buffer;




buffer = intArr[j];




intArr[j] = intArr[j+1];




intArr[j+1] = buffer;




}




}




else if (flag == 4)




{




if (compareFirstName(intArr[j], intArr[j+1]) == -1)




{




InternationalStudent buffer;




buffer = intArr[j];




intArr[j] = intArr[j+1];




intArr[j+1] = buffer;




}




}




else if (flag == 5)




{




if (compareLastName(intArr[j], intArr[j+1]) == -1)




{




InternationalStudent buffer;




buffer = intArr[j];




intArr[j] = intArr[j+1];




intArr[j+1] = buffer;




}




}




else




{




cout << "Invalid sorting flag, please try again " << endl;




return 0;




}




}




}




return length; //Return the length (either the same as before, or new from the overall sort)




}
*/



Student::Student()

{

}




Student::Student(string fn, string ln, float cg, int re, int id):first_name(fn), last_name(ln), student_cgpa(cg), research_score(re), id_number(id)

{




}




DomesticStudent::DomesticStudent()

{

}






















DomesticStudent::DomesticStudent(DomesticStudent* next,string pr, string fn, string ln, float cg, int re, int id):link(next),student_province(pr), Student(fn, ln, cg, re, id)




{




//void DomesticStudent::DomesticStudent(domstudptr &ptr, std::__1::string fn,




// std::__1::string ln, std::__1::string pr, float cg, int re, int id)




}

























//Set, get and other functions for the provinces (unique to domestic student)




void DomesticStudent::setProvince(string pr) {




student_province = pr; //Set Provinces




}




string DomesticStudent::getProvince() {




return student_province; //Return Provinces




}










void DomesticStudent::setLink(DomesticStudent *next)




{




link=next;




}




DomesticStudent* DomesticStudent::getLink() const




{




return link;




}










//BIG 3 IMPLEMENTATION try




//might not need these but if we do, it would only be for a pointer




//so we would only need it for the *link

/*

void DomesticStudent::operator =(const DomesticStudent& right_side)




{




int new_length=strlen(right_side.link);




if ((new_length)>link_length)




{




new_length=link_length;




}




for (int i=0; i<new_length;i++)




{




link[i]=right_side.link[i];




}




// link[new_length]='/0';










}










DomesticStudent::~DomesticStudent()




//deleting domestic student members and Student members




{

*/

/* delete student_province;




delete first_name;




delete last_name;




delete student_cgpa;




delete research_score;




delete id_number;*/










//do we only do it for our link? because its the only thing with a pointer

/*

delete *link;




}




//copy constructor definition




DomesticStudent::DomesticStudent(const DomesticStudent& link_object):max_length(link_object.length())




{




link=new char[max_length+1];




strcpy(link, link_object.link);




}













*/



































































//Set and get functions for the countries (unique to international student)




void InternationalStudent::setCountry(string co) {




student_country = co; //Set Countries




}




string InternationalStudent::getCountry() {




return student_country; //Return Countries




}


































//Below it performs error checking to make sure that the scores of the four components of toefl score are valid




//if invalid, output an error statement and terminate the program




//if the scores are valid store the values into the corresponding array










void InternationalStudent::ToeflScore::setReading(int rea) {




if ((rea < 0) || (rea > 30)) {




cout << endl << "Student has an illegal reading score, correct file and run again.\n";




exit(0);




}




else {




reading_score = rea; //Reading




}




}




void InternationalStudent::ToeflScore::setListening(int lis) {




if ((lis < 0) || (lis > 30)) {




cout << endl << "Student has an illegal listening score, correct file and run again.\n";




exit(0);




}




else {




listening_score = lis; //Listening




}




}




void InternationalStudent::ToeflScore::setSpeaking(int spe) {




if ((spe < 0) || (spe > 30)) {




cout << endl << "Student has an illegal speaking score, correct file and run again.\n";




exit(0);




}




else {




speaking_score = spe; //Speaking




}




}




void InternationalStudent::ToeflScore::setWriting(int wri) {




if ((wri < 0) || (wri > 30)) {




cout << endl << "Student has an illegal writing score, correct file and run again.\n";




exit(0);




}




else {




writing_score = wri; //Writing




}




}




void InternationalStudent::ToeflScore::setToefl() {




//this member function sums the four scores into a total toefl score




toefl_score = reading_score + listening_score + speaking_score + writing_score;




}






















int InternationalStudent::ToeflScore::getReading() {




return reading_score; //Return Reading Score




}




int InternationalStudent::ToeflScore::getListening() {




return listening_score; //Return Listening Score




}




int InternationalStudent::ToeflScore::getSpeaking() {




return speaking_score; //Return Speaking Score




}




int InternationalStudent::ToeflScore::getWriting() {




return writing_score; //Return Writing Score




}




//The get function for the ToeflScore simply returns the requested student's toefl score sum (based on the input student number)




int InternationalStudent::ToeflScore::getToefl() {




return toefl_score; //Return the sum, which is simply the toefl score




}


























































// << operator overloading:




// By entering cout << array[___]; we can now print all the following information










//Domestic Student printout




ostream& operator << (ostream& output, DomesticStudent& d) {










output << endl




<< "Domestic student " << d.getID() << " " << d.getFN()




<< " " << d.getLN() << " from " << d.getProvince()




<< " province has cgpa of "




<< d.getCGPA() << ", and a research score of "




<< d.getResearch() << endl;










}










//International Student printout




ostream& operator << (ostream& output2, InternationalStudent& i) {




output2 << endl




<< "International student " << i.getID() << " "




<< i.getFN() << " "




<< i.getLN() << " from "




<< i.getCountry() << " has cgpa of "




<< i.getCGPA() << ", a research score of "




<< i.getResearch() << ", and the following Toefl Scores:" << endl;










//ToeflScore Get functions:




cout << "Reading: " << i.personalScore.getReading() << endl;




cout << "Listening: " << i.personalScore.getListening() << endl;




cout << "Speaking: " << i.personalScore.getSpeaking() << endl;




cout << "Writing: " << i.personalScore.getWriting() << endl;




cout << "Total: " << i.personalScore.getToefl() << endl;










}
































































//Defining empty constructor functions in the cpp file scope,




//If these were not here, the member functions would not recognize the classes, variables, and function calls










InternationalStudent::ToeflScore::ToeflScore() {




//No constructor function needed here




}







/*

Student::Student() {




//No constructor function needed here




}

*/



















InternationalStudent::InternationalStudent()




{




//No constructor function needed here




}
















////==========================================
















//updated









void headInsert(domstudptr& head, string h_first_name, string h_last_name, string h_student_province,float h_student_cgpa, int h_research_score, int h_id_number )

{
//using a tempPtr so we don't lose any nodes
//the tempPtr will hold the value of the head so head can change
//to new data

domstudptr tempPtr;

tempPtr= new DomesticStudent( head,  h_first_name,  h_last_name,  h_student_province,  h_student_cgpa, h_research_score,  h_id_number);
//tempPtr->setLink(head);
head=tempPtr;
cout<<head->getFN()<<endl;

}

/*old display

void DomesticStudent::Display(DomesticStudent*& head, DomesticStudent*& curr)
{
	curr=head;
	while (curr!=NULL)
	{
		cout<<curr->getFN()<<endl;
	}
}

*/

void printList(DomesticStudent* head)
{
    DomesticStudent* tmp= head;
    while (tmp != NULL)
    {
        cout<<tmp->getFN()<<" "<<tmp->getLN()<<endl;
        tmp=tmp->getLink();
    }
}
/*

void insert(domstudptr afterMe, string i_first_name, string i_last_name, string i_student_province, float i_student_cgpa, int i_research_score, int
i_id_number)




{
domstudptr tempPtr;
tempPtr= new DomesticStudent;

tempPtr->setLink(afterMe->getLink());

tempPtr->setFN(i_first_name);

tempPtr->setLN(i_last_name);




tempPtr->setProvince(i_student_province);




tempPtr->setCGPA(i_student_cgpa);




tempPtr->setResearch(i_research_score);




tempPtr->setID(i_id_number);










afterMe->setLink(tempPtr);










}
*/

void insert(domstudptr afterMe, string i_first_name, string i_last_name, string i_student_province, float i_student_cgpa, int i_research_score, int i_id_number)




{




domstudptr tempPtr;


tempPtr= new DomesticStudent;
//tempPtr->setLink(NULL);

tempPtr->setFN(i_first_name);

tempPtr->setLN(i_last_name);

tempPtr->setProvince(i_student_province);

tempPtr->setCGPA(i_student_cgpa);

tempPtr->setResearch(i_research_score);

tempPtr->setID(i_id_number);
tempPtr->setLink(afterMe->getLink());
afterMe->setLink(tempPtr);
/*
    domstudptr temp=head;

    while (temp->getLink()!=NULL)

    {

        temp=temp->getLink(); //going to end of list

    }*/

   




// tempPtr->setLink(afterMe);
}



//




/// SEARCHING linked list




//










/*




domstudptr search(domstudptr head, string s_first_name, string s_last_name, float s_student_cgpa, int s_research_score, int s_id_number, string s_student_province )




{




domstudptr currentnode=head;







if (currentnode==NULL)




{




return NULL;




}




else




{




//go through array length




//if its not the target and it's not the last node, keep going




while ((currentnode->data)!=targStudent && (currentnode->link)!=NULL)




{




//keep going through LL




currentnode=currentnode->link;




}




if (currentnode->data==targStudent)




{




return currentnode;




}




else 




{




return NULL;




}




}







}*/


//recent changes
DomesticStudent *createNode(string h_first_name, string h_last_name, string h_student_province, float h_student_cgpa, int
h_research_score, int h_id_number)
{
	DomesticStudent* newNode= new DomesticStudent(NULL,  h_first_name,  h_last_name,  h_student_province,  h_student_cgpa, h_research_score,  h_id_number);
    return newNode;
}

//STILL WORKING ON THESE FUNCTION DEFS



/*
int compareResearchScore(Student research1, Student research2) 
{
//declare three returns options: greater (1), smaller (-1) or equal (0)
//between the two students' Research scores
	if (research1.getResearch() == research2.getResearch()) 
	{
		return 0;
	}
	else if (research1.getResearch() > research2.getResearch()) 
	{
		return 1;
	}
	else 
	{
		return -1;
	}
}*/


void overallSort(DomesticStudent personA, DomesticStudent personB )
{//person A is first in the list


if(compareResearchScore(personA, personB) == -1)
	{
	//if the score is greater than the score in the next array entry
	//swap


	

	}/*
else if(compareResearchScore(domArr[j], domArr[j+1]) == 0)
	{
	//if they have the same research scores then sort them by CGPAs
		if (compareCGPA(domArr[j], domArr[j+1]) == -1)
		{
		//if the score is greater than the score in the next array entry
		//swap
		DomesticStudent buffer;
		buffer = domArr[j];
		domArr[j] = domArr[j+1];
		domArr[j+1] = buffer;
		}
		else if(compareCGPA(domArr[j], domArr[j+1]) == 0)
		{
		//if they further have the same CGPA then sort them by province
		if (compareProvince(domArr[j], domArr[j+1]) == -1)
		{
		//if the score is greater than the score in the next array entry
		//swap
		DomesticStudent buffer;
		buffer = domArr[j];
		domArr[j] = domArr[j+1];
		domArr[j+1] = buffer;
		}
		}
    }*/
}

void swapNodes(DomesticStudent* personA, DomesticStudent* personB)
{
    	domstudptr temp;

	temp->setFN(personA->getFN());
    temp->setLN(personA->getLN());
    temp->setProvince(personA->getProvince());
    temp->setCGPA(personA->getCGPA());
    temp->setResearch(personA->getResearch());
    temp->setID(personA->getID());

    personA->setFN(personB->getFN());
    personA->setLN(personB->getLN());
    personA->setProvince(personB->getProvince());
    personA->setCGPA(personB->getCGPA());
    personA->setResearch(personB->getResearch());
    personA->setID(personB->getID());

    personB->setFN(temp->getFN());
    personB->setLN(temp->getLN());
    personB->setProvince(temp->getProvince());
    personB->setCGPA(temp->getCGPA());
    personB->setResearch(temp->getResearch());
    personB->setID(temp->getID());

}



void sortedInsert(DomesticStudent** head_mod, DomesticStudent* new_node)
{
    DomesticStudent* curr;
     if (*head_mod == NULL || (*head_mod)->getResearch() >= new_node->getResearch())  
    {  
        new_node->setLink(*head_mod);  
        *head_mod = new_node;  
    } 
    else
    {
      /* Locate the node before the point of insertion */
        curr = *head_mod;  
        while (curr->getLink()!=NULL &&  
            curr->getLink()->getResearch() < new_node->getResearch())  
        {  
            curr = curr->getLink();  
        }  
        new_node->setLink(curr->getLink());  
        curr->setLink(new_node);  
        
    }
    
}
