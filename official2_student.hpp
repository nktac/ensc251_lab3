//header file student.hpp to declare your classes

#ifndef _HEADER_FILE

#define _HEADER_FILE

using namespace std; //use namespace std

#include <string> //you will have to use string in C++

#include <iostream>




class Student {

public:

//Class Student constructors

Student(string fn, string ln, float cg, int re, int id);

//Student(string fn, string ln, string co, float cg, int re, int rea, int lis, int spe, int wri);

Student();




//Friend Functions for compare functions:

friend int compareCGPA(Student cgpa1, Student cgpa2);

friend int compareResearchScore(Student research1, Student research2);

friend int compareFirstName(Student FN1, Student FN2);

friend int compareLastName(Student LN1, Student LN2);




//Set and Get functions:

void setID(int student_number);

void setFN(string fn);

void setLN(string ln);

void setCGPA(float cg);

void setResearch(int re);

int getID();

string getFN();

string getLN();

float getCGPA();

int getResearch();




private:

//Private variables (arrays). The student class contains first names, last names, cgpas,

//research scores, and id numbers. As the student number increases, the array entry increases.

string first_name;

string last_name;

float student_cgpa;

int research_score;

int id_number;

};




class DomesticStudent : public Student {

public:

//the constructor used is the default since there are no constructor functions required.

 

//Originally they didn't use constructors, but i added them because i think we need

//them for our insertnode functions later

DomesticStudent(DomesticStudent* next, string pr,string fn, string ln, float cg, int re, int id);

DomesticStudent();




friend int compareProvince(DomesticStudent P1, DomesticStudent P2); //Province compare function

friend ostream& operator << (ostream& output, DomesticStudent& d); // << operator overloading, found in cpp\






 




//Get and Set functions for the province (unique to domestic student)

void setProvince(string pr);

string getProvince();

//NODE get and set functions

DomesticStudent *getLink() const;

void setLink(DomesticStudent *next);

void Display(DomesticStudent *& head, DomesticStudent*& curr);

/*
DomesticStudent *getHead() const;
void setHead(DomesticStudent *head_in);

DomesticStudent *getTail() const;
void setTail(DomesticStudent *tail_in);
*/
/*
//BIG 3 IMPLEMENTATION

// Can ignore for now

//assignment operator

//haven't really figured this out yet

void operator =(const DomesticStudent& right_side);




//destructor

~DomesticStudent();




//copy constructor

//DomesticStudent(const DomesticStudent& link_object);

//const so we can't change its content

//creating independent copy of argument

*/



private:

//other vars inherited

string student_province; //Provinces array

DomesticStudent * link;
//domstudptr head;




 

};

//pointer to domestic student

typedef DomesticStudent* domstudptr;

//for testing, only sorting based on research score
void sortedInsert(DomesticStudent** head_mod, DomesticStudent* new_node);


class DomStudList
{
	private:
	domstudptr head, tail;

	public:
	DomStudList()
	{
		head=NULL;
		tail=NULL;
	}

	void overallSort(DomesticStudent personA, DomesticStudent personB);
	void swapNodes(DomesticStudent* personA, DomesticStudent* personB);

};







class InternationalStudent : public Student {

public:

//the constructor used is the default since there are no constructor functions required.

//InternationalStudent(string fn, string ln, string co, float cg, int re, int rea, int lis, int spe, int wri);

InternationalStudent();




friend int compareCountry(InternationalStudent C1, InternationalStudent C2); //Country compare function

friend ostream& operator << (ostream& output2, InternationalStudent& i); // << operator overloading, found



//Get and Set functions for the country (unique to international student)

void setCountry(string co);

string getCountry();




class ToeflScore {

public:

//the constructor used is the default since there are no constructor functions required.

//ToeflScore(int rea, int lis, int spe, int wri);

ToeflScore();




//Toefl score has get functions which takes the student number (for array entry) and respective scores.

//Results are stored in the private variables below.

void setReading(int rea); //Reading

void setListening(int lis); //Listening

void setSpeaking(int spe); //Speaking

void setWriting(int wri); //Writing

void setToefl(); //Total sum

//The get functions return the reequested int

int getReading(); //Reading

int getListening(); //Listening

int getSpeaking(); //Speaking

int getWriting(); //Writing

int getToefl(); //Total sum

private:

//Private variable arrays

int reading_score;

int listening_score;

int speaking_score;

int writing_score;

int toefl_score;

};




ToeflScore personalScore; //Defining an object that will handle the toeflscore information for all inter student objects

private:

string student_country; //Countries array

};




void DomBubbleSort(DomesticStudent domArr[], size_t length, size_t flag); //Domestic Student Bubble Sort function

int IntBubbleSort(InternationalStudent intArr[], unsigned long int length,
unsigned long int
flag); //Interntional Student Bubble Sort




////========================================================================

// linked list non member functions







//head insert function

void headInsert(domstudptr& head, string h_first_name, string h_last_name, string h_student_province, float h_student_cgpa, int
h_research_score, int
h_id_number);

void insert(domstudptr afterMe, string h_first_name, string h_last_name, string h_student_province, float h_student_cgpa, int
h_research_score, int h_id_number);



domstudptr search(domstudptr head, string s_first_name, string s_last_name, float s_student_cgpa, int s_research_score, int
s_id_number, string s_student_province ); 


//recent changes
//void createNode(domstudptr& head, string h_first_name, string h_last_name, string h_student_province, float h_student_cgpa, int h_research_score, int h_id_number);
DomesticStudent * createNode(string h_first_name, string h_last_name, string h_student_province, float h_student_cgpa, int h_research_score, int h_id_number);


void printList(DomesticStudent* head);




#endif
