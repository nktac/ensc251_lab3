char uiStudentChoice;
char uiFunctionChoice;
char uiSearchChoice;
int uiSearchID;
double uiSearchCGPA;
int uiSearchRS;
string uiSearchFN;
string uiSearchLN;
string uiDeleteFN;
string uiDeleteLN;

//user picks Domestic Students or International Students
cout << "Would you like to work with Domestic Students or International Students? Enter the appropriate letter." << endl;
cout << "D: Domestic Students" << endl << "I: International Students" <<  endl;
 cout << "Enter: ";
cin >> uiStudentChoice;

//if user picks Domestic Students
if (uiStudentChoice == 'D' || uiStudentChoice == 'd')
{
  cout << "What would you like to do with Domestic Students? Enter the appropriate letter." << endl;
  cout << "S: Search" << endl << "C: Create" << endl << "D: Delete" << endl;
  cout << "Enter: ";
  cin >> uiFunctionChoice;
  
  if (uiFunctionChoice == 'S' || uiFunctionChoice == 's')
  {
    //search
    cout << "What would you like to search? Enter the appropriate letter." << endl;
    cout << "A: Application ID" << endl << "C: CGPA" << endl << "R: Research Score" << "N: Name" << endl;
  cout << "Enter: ";
    cin >> uiSearchChoice;
    
    if (uiSearchChoice == 'A' || uiSearchChoice == 'a')
    {
      //search app id
      cout << "Search Application ID: ";
      cin >> uiSearchID;
      //check for matching ID
      //print found students or no match
    }
    else if (uiSearchChoice== 'C' || uiSearchChoice == 'c')
    {
      //search cgpa
      cout << "Search CGPA: ";
      cin >> uiSearchCGPA;
      //check for matching CGPA
      //print found students or no match
    }
    else if (uiSearchChoice =='R' || uiSearchChoice == 'r')
    {
      //search research score
      cout << "Search Research Score: ";
      cin >> uiSearchRS;
      //check for matching research score
      //print found students or no match
    }
    else if (uiSearchChoice == 'N' || uiSearchChoice == 'n')
    {
      //search name
      cout << "Search First Name: ";
      cin >> uiSearchFN;
      cout << endl << "Search Last Name: ";
      cin >> uiSearchLN;
      //check for matching name
      //print found students or no match
    }
    else
    {//if user did not enter appropriate letter for search choice
      //invalid input
      cout << "Invalid input. Exiting." << endl;
      return 0;
    }
  }
  else if (uiFunctionChoice == 'C' || uiFunctionChoice == 'c')
  {
    //create
    cout << "Create New Domestic Student." << endl;
    //cin all variables
    //insert node in linked list
    //sort updated linked list
  }
  else if (uiFunctionChoice == 'D' || uiFunctionChoice == 'd')
  {
    //delete
    cout << "Delete Domestic Student." << endl;
    cout << "Delete Student First Name: ";
    cin >> uiDeleteFN;
    cout << "Delete Student Last Name: ";
    cin >> uiDeleteLN;
    //delete matching object
  }
  else
  {//if user did not enter appropriate letter for function choice
    cout << "Invalid input. Exiting." << endl;
    return 0;
  }
}
//if user picks International Students
else if (uiStudentChoice == 'I' || uiStudentChoice == 'i')
{
  cout << "What would you like to do with International Students? Enter the appropriate letter." << endl;
  cout << "S: Search" << endl << "C: Create" << endl << "D: Delete" << endl;
  cout << "Enter: ";
  cin >> uiFunctionChoice;
  
  if (uiFunctionChoice == 'S' || uiFunctionChoice == 's')//if use picks search
  {
    //search
    cout << "What would you like to search? Enter the appropriate letter." << endl;
    cout << "A: Application ID" << endl << "C: CGPA" << endl << "R: Research Score" << "N: Name" << endl;
    cout << "Enter: ";
    cin >> uiSearchChoice;
    
    if (uiSearchChoice == 'A' || uiSearchChoice == 'a')//if user picks search application ID
    {
      //search app id
      cout << "Search Application ID: ";
      cin >> uiSearchID;
      //check for matching ID
      //print found students or no match
    }
    else if (uiSearchChoice== 'C' || uiSearchChoice == 'c')//if user picks search cgpa
    {
      //search cgpa
      cout << "Search CGPA: ";
      cin >> uiSearchCGPA;
      //check for matching CGPA
      //print found students or no match
    }
    else if (uiSearchChoice == 'R' || uiSearchChoice == 'r')//if user picks search research score
    {
      //search research score
      cout << "Search Research Score: ";
      cin >> uiSearchRS;
      //check for matching research score
      //print found students or no match
    }
    else if (uiSearchChoice == 'N' || uiSearchChoice == 'n')//if user picks search by name
    {
      //search name
      cout << "Search First Name: ";
      cin >> uiSearchFN;
      cout << endl << "Search Last Name: ";
      cin >> uiSearchLN;
      //check for matching name
      //print found students or no match
    }
    else//if user did not enter appropriate letter for search choice
    {
      //invalid input
      cout << "Invalid input. Exiting." << endl;
      return 0;
    }
  }
  else if (uiFunctionChoice == 'C' || uiFunctionChoice == 'c')//if user chose to create
  {
    //create
    cout << "Create New Domestic Student." << endl;
    //cin all variables
    //insert node in linked list
    //sort updated linked list
  }
  else if (uiFunctionChoice == 'D' || uiFunctionChoice == 'd')//if user chose to delete
  {
    //delete
    cout << "Delete Domestic Student." << endl;
    cout << "Delete Student First Name: ";
    cin >> uiDeleteFN;
    cout << "Delete Student Last Name: ";
    cin >> uiDeleteLN;
    cout << endl;
    //delete matching object
  }
  else//if user did not enter appropriate letter for function choice
  {
    cout << "Invalid input. Exiting." << endl;
    return 0;
  }
}
//if invalid input
else//if user did not enter appropriate letter for student choice
{
    cout << "Invalid input. Exiting." << endl;
    return 0;
}